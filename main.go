package main

import (
	"fmt"
	"github.com/mangtasTest/logger"
	"github.com/mangtasTest/rest"
	"github.com/mangtasTest/service"
)

func main() {
	logger.Init()

	logger.Instance().Info("#==================================#")
	logger.Instance().Info("#===========Starting Server =======#")
	logger.Instance().Info("#==================================#")

	serviceContainer := service.NewServiceContainer()

	/*
	* Initiate Rest Server
	 */
	rest.StartServer(serviceContainer)

	fmt.Println("========== Rest Server Started ============")
	logger.Instance().Println("========== Server Stated ============")
	select {}

}
