package service

type Container struct {
	CounterService   CounterService
	GbeConfigService GbeConfigService
	LoggerService    LoggerService
}

// creates a container by initiating all required services
func NewServiceContainer() *Container {
	//Get Configs
	//gbeConfig := conf.GetConfig()
	gbeConfigService := NewGbeConfigService()

	//Get Logger Service
	loggerService := NewLoggerService()

	//Get User Service
	counterService := NewCounterService(gbeConfigService, loggerService)

	return &Container{
		GbeConfigService: gbeConfigService,
		LoggerService:    loggerService,
		CounterService:   counterService,
	}
}
