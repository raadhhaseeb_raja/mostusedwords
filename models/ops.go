package models

import (
	"github.com/bbalet/stopwords"
	"log"
	"sort"
	"strings"
)

// CountSubStrings counts the substrings by the occurrences and stores
// each element/substring as a key and also saves occurrences as value
func CountSubStrings(inputString string) map[string]int {
	// inputStringToArray saves each substring separately as string in an array
	inputStringToArray := strings.Fields(inputString)
	log.Println(inputStringToArray)
	// initializing a map
	wordsWithOccurrences := make(map[string]int)
	for _, word := range inputStringToArray {
		// checks if word exists in the map
		_, ok := wordsWithOccurrences[word]
		if ok { // if word exist in our map, we increment the occurrence
			wordsWithOccurrences[word] += 1
		} else { // if word does not exist in our map, we add it to map and initialize occurrence as 1
			wordsWithOccurrences[word] = 1
		}
	}
	// return
	return wordsWithOccurrences
}

// RemoveStopWords removes stop words from the given string. It uses a library to remove stop words.
// stop words are limited by library and a custom function can be used instead to improve removal
func RemoveStopWords(inputString string) string {
	// returns cleaned string after removing stop word
	return stopwords.CleanString(inputString, "", false)
}

// ToLower converts the input string to lower case.
func ToLower(inputString string) string {
	return strings.ToLower(inputString)
}

// SortAndCut sorts the map[string]int by adding them to a struct and
// then appends the struct to another struct that only takes in the top 10 sorted values by descending order
func SortAndCut(wordsWithOccurrences map[string]int) (sortedWithOccurrences []SortedKeyVal) {
	// array of struct KeyVal that has a field Key and a field val
	var keyVal []KeyVal
	// we range over our input map and save the key in map to key field in our struct
	// and value in map to value in our struct.
	// the struct is appended with itself
	for key, val := range wordsWithOccurrences {
		keyVal = append(keyVal, KeyVal{
			Key:   key,
			Value: val,
		})
	}
	// using internal sort package to sort slices
	// it sorts the keyVal in descending order e.g. every first value must be greater than second value
	sort.Slice(keyVal, func(i, j int) bool {
		return keyVal[i].Value > keyVal[j].Value
	})
	// initializing another array to store top 10 values
	var sortedKeyVal []SortedKeyVal
	// range over keyVal array of struct, append the index and element in KeyVal to
	// sortedKeyVal array until total of 10 values have been saved.
	for index, element := range keyVal {
		if index == 10 {
			break
		}
		sortedKeyVal = append(sortedKeyVal, SortedKeyVal{
			Key: element.Key,
			Val: element.Value,
		})
	}
	// returns
	return sortedKeyVal
}
