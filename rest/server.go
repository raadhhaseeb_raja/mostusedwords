package rest

import (
	"github.com/go-chi/chi"
	"github.com/go-chi/cors"
	"github.com/mangtasTest/conf"
	"net/http"
	"time"
)

type HttpServer struct {
	addr              string
	counterController CounterController
}

//NewHttpServer create server instance
func NewHttpServer(addr string,
) *HttpServer {
	return &HttpServer{
		addr: addr,
	}
}

func (s HttpServer) GetCounterController() CounterController {
	return s.counterController
}

func (server *HttpServer) Start() {
	r := chi.NewRouter()

	r.Use(cors.Handler(cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowedMethods:   []string{"GET", "POST", "PUT", "PATCH", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"*"},
		ExposedHeaders:   []string{"Content-Length"},
		AllowCredentials: true,
		MaxAge:           int(12 * time.Hour),
	}))

	r.Route("/", func(r chi.Router) {
		r.Post("/topTenMostUsedWords", server.counterController.GetInputFile)
	})

	//port := os.Getenv("PORT")
	//if port == "" {
	//	port = conf.GetConfig().RestServer.Addr // Default port if not specified
	//}

	//err := http.ListenAndServe(":"+port, r)

	err := http.ListenAndServe(conf.GetConfig().RestServer.Addr, r)
	if err != nil {
		panic(err)
	}

}
