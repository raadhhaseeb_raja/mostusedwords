package rest

import (
	"testing"
)

// Creating mock test cases with dummy inputs to verify if correct data is returned
func TestDifferentInputStrings(t *testing.T) {
	t.Run("Should return KeyVal", func(t *testing.T) {
		testService := NewTestService(t)
		mockCorrectInput := GetMockCorrectInputRequest()
		mockService := testService.GetMockCounterService()
		mockKeyVal := GetMockSortedKeyVal()
		mockService.EXPECT().GetInputFile(mockCorrectInput, false).Return(mockKeyVal, nil).AnyTimes()

	})
}
