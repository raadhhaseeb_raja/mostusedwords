package rest

import (
	"bytes"
	"github.com/mangtasTest/models"
	"github.com/mangtasTest/service"
	"io"
	"net/http"
)

type CounterController interface {
	GetInputFile(w http.ResponseWriter, r *http.Request)
}

type counterController struct {
	counterService service.CounterService
}

func NewStringsController(userService service.CounterService) CounterController {
	return &counterController{
		counterService: userService,
	}
}

func (s counterController) GetInputFile(w http.ResponseWriter, r *http.Request) {
	// Get inputFile from Form
	inputFile, _, err := r.FormFile("textFile")
	// if err, return an error
	if err != nil {
		WriteErrorJson(w, models.NewStandardResponse(false, 400, "Bad Request", nil), http.StatusOK)
	}
	// initialize new buffer
	buf := bytes.NewBuffer(nil)
	// copy inputFile to initialized buffer until EOF is reached, if err, return an error
	if _, err := io.Copy(buf, inputFile); err != nil {
		WriteErrorJson(w, models.NewStandardResponse(false, 500, "Internal Server Error", nil), http.StatusOK)
	}
	// convert the buffered objet to string
	fileData := string(buf.Bytes())
	var excludeStopWords bool

	// get form value to see if stop words need to be included or excluded
	// if true, stop words will be excluded, else they will be included
	willExcludeStopWords := r.FormValue("excludeStopWords")
	if willExcludeStopWords == "true" {
		excludeStopWords = true
	} else {
		excludeStopWords = false
	}
	// save the string to a struct
	text := models.InputRequest{Input: fileData}
	//log.Println(text)
	// calling the GetInputFile function.
	// If sr ( standard response) is returned, we throw an error, else we write a success message
	resp, sr := s.counterService.GetInputFile(text, excludeStopWords)
	if sr != nil {
		WriteErrorJson(w, sr, http.StatusOK)
		return
	}
	//success
	WriteJson(w, models.NewStandardResponse(true, 1000, "Success", resp), http.StatusOK)
}
