package rest

import (
	"github.com/golang/mock/gomock"
	"github.com/mangtasTest/mocks/service"
	"github.com/mangtasTest/models"
	"testing"
)

type TestService interface {
	GetMockCounterService() *service.MockCounterService
}

type testService struct {
	ctrl *gomock.Controller
}

func (t *testService) GetMockCounterService() *service.MockCounterService {
	return service.NewMockCounterService(t.ctrl)
}

func NewTestService(t *testing.T) TestService {
	mockCtrl := gomock.NewController(t)
	return &testService{
		ctrl: mockCtrl,
	}
}

// Mocked response
func GetMockCorrectInputRequest() models.InputRequest {
	return models.InputRequest{
		Input: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur elementum placerat nulla in dapibus. Vestibulum luctus turpis vitae neque eleifend, ac fermentum sem consectetur. Aenean at scelerisque ipsum. Praesent tristique, metus ac viverra auctor, massa nibh pretium nisl, at gravida lorem massa sed ipsum. Aenean fermentum, arcu ut pharetra iaculis, felis est fermentum lectus, rhoncus auctor lacus neque vitae ligula. Donec eu urna nec erat volutpat porta sed eu mauris. Morbi nec ipsum lorem. Vestibulum lorem felis, feugiat eu orci nec, hendrerit accumsan felis. Suspendisse in quam vulputate est ultrices semper.\n\nAliquam rutrum neque in ipsum mollis, in elementum augue consectetur. Donec aliquam mi nec auctor aliquam. Donec dapibus nulla vitae mi maximus commodo. In tellus dui, interdum vitae quam eu, vulputate convallis tortor. Pellentesque in odio sed felis posuere accumsan ac non felis. Maecenas at pharetra mauris. Duis gravida, dui feugiat consequat tincidunt, justo orci egestas eros, nec tristique arcu leo id metus.\n\nIn ut bibendum tellus, a convallis nisl. Nulla pulvinar enim eget sapien volutpat, ut luctus ligula malesuada. Aenean sit amet ullamcorper ex. Pellentesque et fermentum tellus. Fusce mollis libero ut nulla pellentesque, eu efficitur velit rhoncus. Phasellus ac nibh tellus. Phasellus vulputate dolor velit, quis semper.",
	}
}

// Mocked standard response
func GetMockStandardResp() *models.StandardResponse {
	return models.NewStandardResponse(false, 400, "String is invalid", nil)
}

func GetMockSortedKeyVal() *[]models.SortedKeyVal {
	return &[]models.SortedKeyVal{}
}
