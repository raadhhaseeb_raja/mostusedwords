package rest

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

// WriteJson is Custom output
func WriteJson(w http.ResponseWriter, v interface{}, header int) {
	marshalledJson, e := json.MarshalIndent(v, "", "    ")
	if e != nil {
		WriteErrorJson(w, fmt.Sprintf("unable to marshal json with indentation: %s", e), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(header)
	_, err := w.Write(marshalledJson)
	if err != nil {
		return
	}
}

// WriteErrorJson is Custom output
func WriteErrorJson(w http.ResponseWriter, v interface{}, header int) {
	w.WriteHeader(header)

	marshalledJson, e := json.MarshalIndent(v, "", "    ")
	if e != nil {
		log.Printf("unable to marshal json with indentation: %s\n", e)
		w.Write([]byte(fmt.Sprintf("unable to marshal json with indentation: %s\n", e)))
		return
	}
	_, err := w.Write(marshalledJson)
	if err != nil {
		return
	}
}
