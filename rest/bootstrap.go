package rest

import (
	"github.com/mangtasTest/logger"
	"github.com/mangtasTest/service"
)

func StartServer(container *service.Container) *HttpServer {
	// Inject services instance from ServiceContainer

	// Initializing Controllers
	counterController := NewStringsController(container.CounterService)

	//Initializing http server
	httpServer := NewHttpServer(
		container.GbeConfigService.GetConfig().RestServer.Addr,
	)

	//Inject controller instance to server
	httpServer.counterController = counterController
	go httpServer.Start()
	logger.Instance().Info("rest server ok")
	return httpServer
}
